import { group, sleep } from 'k6';
import http from 'k6/http';

// Version: 1.2
// Creator: WebInspector

export let options = { 
	maxRedirects: 0, 
    iterations: 1500,
    vus: 100,
    stages: [
      { duration: "5m", target: 1500 },
      { duration: "10m" },
      { duration: "5m", target: 0 }
    ],
    throw: true,
    noConnectionReuse: true
  };

export default function() {

	group("page_1 - https://accept.delijn.be/", function() {
		let req, res;
		req = [{
			"method": "get",
			"url": "https://accept.delijn.be/",
			"params": {
				"headers": {
					"Host": "accept.delijn.be",
					"Connection": "keep-alive",
					"Pragma": "no-cache",
					"Cache-Control": "no-cache",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5"
				}
			}
		},{
			"method": "get",
			"url": "https://accept.delijn.be/nl/",
			"params": {
				"headers": {
					"Host": "accept.delijn.be",
					"Connection": "keep-alive",
					"Pragma": "no-cache",
					"Cache-Control": "no-cache",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/js/modernizr.custom.js",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "*/*",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/css/screen-3.46.2.css",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "text/css,*/*;q=0.1",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/css/style-3.46.2.css",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "text/css,*/*;q=0.1",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},
		{
			"method": "get",
			"url": "https://accept-static.delijn.be/Images/staking_tcm3-12270.jpg",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/Images/Festival_tcm3-395.png",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/img/spinner.gif",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/Images/vvm-logo-mark_tcm3-1123.svg",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/js/vendor-core-3.46.2.js",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "*/*",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/js/vendor-default-3.46.2.js",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "*/*",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/js/be/delijn/site/core-3.46.2.js",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "*/*",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/js/be/delijn/site/controller-3.46.2.js",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "*/*",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},
		{
			"method": "get",
			"url": "https://accept-static.delijn.be/js/be/delijn/site/modules/noodBericht-3.46.2.js",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "*/*",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/js/be/delijn/site/modules/cookieBanner-3.46.2.js",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "*/*",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/css/print-3.46.2.css",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "text/css,*/*;q=0.1",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},
		{
			"method": "get",
			"url": "https://accept-static.delijn.be/Images/bg-intro-large_tcm3-228.jpg",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/img/icon-caret-down.svg",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept-static.delijn.be/css/screen-3.46.2.css",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/Images/dl-logo_tcm3-341.svg",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		}];
		res = http.batch(req);
		sleep(0.73);
		req = [
			{
			"method": "get",
			"url": "https://b2cservices-accept.delijn.be/rise-api-search/search/nieuwsbox/tcm:3-292-1024/3",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"Origin": "https://accept.delijn.be",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "b2cservices-accept.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "*/*",
					"Cache-Control": "no-cache",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"DNT": "1"
				}
			}
		}
		,{
			"method": "get",
			"url": "https://accept-static.delijn.be/Images/belbus2_tcm3-233.jpg",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/Images/abonnementen_tcm3-11035.jpg",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},
		{
			"method": "get",
			"url": "https://accept-static.delijn.be/favicon.png",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		},{
			"method": "get",
			"url": "https://accept-static.delijn.be/favicon.png",
			"params": {
				"headers": {
					"Pragma": "no-cache",
					"DNT": "1",
					"Accept-Encoding": "gzip, deflate, br",
					"Host": "accept-static.delijn.be",
					"Accept-Language": "en-BE,en;q=0.9,nl-BE;q=0.8,nl;q=0.7,en-GB;q=0.6,en-US;q=0.5",
					"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Referer": "https://accept.delijn.be/nl/",
					"Connection": "keep-alive",
					"Cache-Control": "no-cache"
				}
			}
		}];
		res = http.batch(req);
		// Random sleep between 2s and 4s
		sleep(Math.floor(Math.random()*3+2));
	});

}
